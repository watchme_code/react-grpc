# Go-React Ticket Booking System

## Key Features

- **Go Backend:** 
- **React Frontend:**
- **Type Safety:** 
- **Seamless Integration:** 

### Prerequisites

Before you begin, ensure you have the following installed on your system:
- Go (version 1.x or later)
- Node.js and npm (for React frontend)
- Git (for cloning the repository)

### Installation

1. **Clone the repository**


2. **Setup the Backend**

Navigate to the backend directory, install dependencies, and start the Go server:

```bash
cd backend
go get -u
go run server.go
```

3. **Setup the Frontend**

In a new terminal, go to the frontend directory, install npm packages, and start the React application:

```bash
cd frontend
npm install
npm start
```

The React development server will launch the application in your default web browser.

## Project Structure

- `/backend` - Contains the Go backend application, including REST API endpoints for handling ticket booking operations.
- `/frontend` - Houses the React application for the frontend, including components, services, and state management for the booking system.

## Exploring the Integration

This project is designed to highlight the integration between Go and React, focusing on:
- **API Communication:** How the React client interacts with the Go backend through API calls.
- **Type Safety Benefits:** The advantages of using strongly typed languages in reducing runtime errors and improving code quality.

